fun countVowels(str: String):Int {
    var result = 0

    for (item: Char in str) {
        when (item) {
            'a', 'e', 'i', 'o', 'u'  -> result++
        }
    }

    return result
}

fun main() {
    println(countVowels("Hello"))
}
